const express = require('express');
// CORS IMPORT
const cors = require('cors');

const bodyParser = require('body-parser');
// create express app
const app = express();
// Setup server port
const port = process.env.PORT || 4000;

// Basic Auth Express
const basicAuth = require('express-basic-auth');

const { base64decode } = require('nodejs-base64');

// parse requests of content-type - application/x-www-from-urlencoded
app.use(bodyParser.urlencoded({ extendend: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json());

/**
 * Enabling CORS
 */
app.use(cors({
    origin: 'http://localhost:4200'
}));

// Configuring the database

const dbConfig = require('./config/db.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database

mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log(('Successfully connected to the database'));
}).catch(err => {
    console.log('Could not connect to the database.', err);
    process.exit();
});
app.use((req, res, next) => {
    if (req.headers["authorization"]) {
        const basicAuthHeader = req.headers["authorization"];
        const credentialsBase64 = basicAuthHeader.split(' ');
        const userPassword = base64decode(credentialsBase64[1]);
        const user = userPassword.split(':')[0];
        const password = userPassword.split(':')[1];

        if (user === 'admin' && password === 'supersecret') {
            console.log('Authorized by basic auth');
            next();
            return;
        }
    }
    res.status(401);
    res.send({
        error: "Unauthorized "
    });
});
app.use(basicAuth({
    users: {
        'admin': 'supersecret',
        'adam': 'password1234',
        'eve': 'asdfghjkl',
    }
}));

// Login Route
app.post('/api/login', (req, res) => {
    if (!req.headers['authorization'] || !req.body) {
        console.log(headers);
        res.status(401);
        res.send({
            error: 'Unauthorized'
        });
    }
    res.status(200).send();
});
// define a root/default route

app.get('/', (req, res) => {
    res.json({ 'message': 'Hello World' });
});

const userRoutes = require('./src/routes/user.routes');

app.use('/api/users', userRoutes);

// listen for requests
app.listen(port, () => {
    console.log(`Node server is listening on port ${port}`);
});