# API CRUD MONGO

## Repositiry

### Clone the repository

#### * *_SSH_*
https://gitlab.com/pablohuete/crudexpressmongo.git

#### * *_HTTPS_*
https://gitlab.com/pablohuete/crudexpressmongo.git

### Install Dependencies
```
cd crudexpressmongo/
npm install
```

### Start the Server

```
node app.js
```

### Start the Server with _Nodemon Library_

```
nodemon start
```
